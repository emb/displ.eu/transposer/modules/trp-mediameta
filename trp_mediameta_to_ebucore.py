#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys

# Try to find the utils directory and add it to the search path.
def add_utils_search_path(dir: str) -> bool:
    for p in sys.path:
        if p.endswith('/utils'):
            return True
    while len(dir.split(os.sep)) > 1:
        dir = os.path.dirname(dir)
        t_dir = os.path.join(dir, 'utils')
        if os.path.isdir(t_dir):
            sys.path.insert(0, t_dir)
            return True
    return False
self_dir = os.path.dirname(os.path.realpath(__file__))
if not os.path.isfile(os.path.join(self_dir, 'utils.py')):
    add_utils_search_path(self_dir)

import json
import datetime

import jinja2

import trp_mediainfo





# EBUCore
EBU_CORE_TEMPLATE = {
    'main': '''{
        "ebucore:ebuCoreMain": {
            "@version": "1.8",
            "@writingLibraryName": "MediaInfoLib",
            "@writingLibraryVersion": "21.09",
            "@dateLastModified": "{{ now_date }}",
            "@timeLastModified": "{{ now_time }}",
            "ebucore:coreMetadata": [
                {
                    "ebucore:format": [
                        {
                            "ebucore:videoFormat": [
                                {{ prerender_video }}
                            ],
                            "ebucore:audioFormat": [
                                {{ prerender_audio }}
                            ],
                            "ebucore:containerFormat": [
                                {
                                    "@containerFormatName": "{{ containerFormatName }}",
                                    "@containerFormatVersionId": "{{ containerFormatVersionId }}",
                                    "@containerFormatId": "{{ containerFormatId }}",
                                    "ebucore:containerEncoding": [
                                        {
                                            "@formatLabel": "{{ containerEncoding }}"
                                        }
                                    ],
                                    "ebucore:technicalAttributeString": [
                                        {
                                            "@typeLabel": "WritingApplication",
                                            "#value": "{{ containerWritingApplication }}"
                                        },
                                        {
                                            "@typeLabel": "WritingLibrary",
                                            "#value": "{{ containerWritingLibrary }}"
                                        }
                                    ]
                                }
                            ],
                            "ebucore:dataFormat": [
                                {{ prerender_text }}
                            ],
                            "ebucore:duration": [
                                {
                                    "ebucore:normalPlayTime": [
                                        {
                                            "#value": "{{ durationEBUCore }}"
                                        }
                                    ]
                                }
                            ],
                            "ebucore:fileSize": [
                                {
                                    "#value": "{{ fileSize}}"
                                }
                            ],
                            "ebucore:fileName": [
                                {
                                    "#value": "{{ fileName}}"
                                }
                            ],
                            "ebucore:locator": [
                                {
                                    "#value": "{{ filePath }}"
                                }
                            ],
                            "ebucore:technicalAttributeInteger": [
                                {
                                    "@typeLabel": "OverallBitRate",
                                    "@unit": "{{ overallBitRateUnit }}",
                                    "#value": "{{ overallBitRate}}"
                                }
                            ],
                            "ebucore:dateCreated": [
                                {
                                    "@startDate": "{{ dateCreatedDate }}",
                                    "@startTime": "{{ dateCreatedTime }}"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }''',

    'video': '''{
        "@videoFormatName": "{{ format }}",
        "ebucore:width": [
            {
                "@unit": "pixel",
                "#value": "{{ width }}"
            }
        ],
        "ebucore:height": [
            {
                "@unit": "pixel",
                "#value": "{{ height }}"
            }
        ],
        "ebucore:frameRate": [
            {
                "@factorNumerator": "{{ frameRateNum }}",
                "@factorDenominator": "{{ frameRateDen }}",
                "#value": "{{ frameRateBase }}"
            }
        ],
        "ebucore:aspectRatio": [
            {
                "@typeLabel": "display",
                "ebucore:factorNumerator": [
                    {
                        "#value": "{{ displayAspectRatioNum }}"
                    }
                ],
                "ebucore:factorDenominator": [
                    {
                        "#value": "{{ displayAspectRatioDen }}"
                    }
                ]
            }
        ],
        "ebucore:videoEncoding": [
            {
                "@typeLabel": "{{ encoding }}"
            }
        ],
        "ebucore:codec": [
            {
                "ebucore:codecIdentifier": [
                    {
                        "dc:identifier": [
                            {
                                "#value": "{{ codecID}}"
                            }
                        ]
                    }
                ]
            }
        ],
        "ebucore:bitRate": [
            {
                "#value": "{{ bitRate }}"
            }
        ],
        "ebucore:videoTrack": [
            {
                "@trackId": "{{ id }}"
            }
        ],
        "ebucore:technicalAttributeString": [
            {
                "@typeLabel": "ColorSpace",
                "#value": "{{ colorSpace }}"
            },
            {
                "@typeLabel": "ChromaSubsampling",
                "#value": "{{ chromaSubsampling }}"
            },
            {% if colorDescriptionPresent -%}
            {
                "@typeLabel": "colour_primaries",
                "#value": "{{ colorPrimaries }}"
            },
            {
                "@typeLabel": "transfer_characteristics",
                "#value": "{{ transferCharacteristics }}"
            },
            {
                "@typeLabel": "matrix_coefficients",
                "#value": "{{ matrixCoefficients }}"
            },
            {
                "@typeLabel": "colour_range",
                "#value": "{{ colorRange }}"
            },
            {% endif -%}
            {
                "@typeLabel": "WritingLibrary",
                "#value": "{{ encodedLibraryName }} {{ encodedLibraryVersion }}"
            },
            {
                "@typeLabel": "Default",
                "#value": "{{ 'Yes' if default else 'No' }}"
            },
            {
                "@typeLabel": "Forced",
                "#value": "{{ 'Yes' if forced else 'No' }}"
            }
        ],
        "ebucore:technicalAttributeInteger": [
            {
                "@typeLabel": "BitDepth",
                "@unit": "bit",
                "#value": "{{ bitDepth }}"
            },
            {
                "@typeLabel": "StreamSize",
                "@unit": "byte",
                "#value": "{{ streamSize }}"
            }
        ]
    }''',
    
    'audio': '''{
        "@audioFormatName": "{{ format }}",
        "ebucore:audioEncoding": [
            {
                "@typeLabel": "{{ encoding }}"{% if encodingUrl %},
                "@typeLink": "{{ encodingUrl }}"{% endif %}
            }
        ],
        "ebucore:codec": [
            {
                "ebucore:codecIdentifier": [
                    {
                        "dc:identifier": [
                            {
                                "#value": "{{ codecID }}"
                            }
                        ]
                    }
                ],
                "ebucore:name": [
                    {
                        "#value": "{{ codecIDName }}"
                    }
                ]
            }
        ],
        "ebucore:samplingRate": [
            {
                "#value": "{{ samplingRate }}"
            }
        ],
        "ebucore:bitRate": [
            {
                "#value": "{{ bitRate }}"
            }
        ],
        "ebucore:bitRateMode": [
            {
                "#value": "{{ 'constant' if bitRateMode == 'CBR' else 'variable' }}"
            }
        ],
        "ebucore:audioTrack": [
            {
                "@trackId": "{{ id }}",
                "@trackLanguage": "{{ languageCode }}"
            }
        ],
        "ebucore:channels": [
            {
                "#value": "{{ channels }}"
            }
        ],
        "ebucore:technicalAttributeString": [
            {
                "@typeLabel": "ChannelPositions",
                "#value": "{{ channelPositions }}"
            },
            {
                "@typeLabel": "ChannelLayout",
                "#value": "{{ channelLayout }}"
            },
            {
                "@typeLabel": "Endianness",
                "#value": "{{ formatSettingsEndianness }}"
            }
        ],
        "ebucore:technicalAttributeInteger": [
            {
                "@typeLabel": "StreamSize",
                "@unit": "{{ streamSizeUnit }}",
                "#value": "{{ streamSize }}"
            }
        ]
    }''',
    
    'text': '''{
        "@dataFormatName": "{{ format }}",
        "@dataTrackId": "{{ id }}",
        "ebucore:captioningFormat": [
            {
                "@captioningFormatName": "{{ format }}",
                "@trackId": "{{ id }}",
                {% if typeLabel %}"@typeLabel": "{{ typeLabel }}",{% endif %}
                "@language": "{{ languageCode }}"
            }
        ],
        "ebucore:codec": [
            {
                "ebucore:codecIdentifier": [
                    {
                        "dc:identifier": [
                            {
                                "#value": "{{ codecID }}"
                            }
                        ]
                    }
                ]
            }
        ]
    }'''
}





class TrpMediametaToEBUCore:
    
    @classmethod
    def convert(cls, data: dict) -> dict:
        dt_now = datetime.datetime.now()
        environment = jinja2.Environment()
        
        info_data = {
            'now_date': dt_now.strftime('%Y-%m-%d'),
            'now_time': dt_now.strftime('%H:%M:%SZ')
        }
        if 'info' in data:
            info_data.update(data['info'])


        # Video streams
        template = environment.from_string(
            EBU_CORE_TEMPLATE['video']
        )
        container = []
        for obj in data['video']:
            container.append(
                template.render(
                    **obj
                )
            )
        info_data['prerender_video'] = ', '.join(container)


        # Audio streams
        template = environment.from_string(
            EBU_CORE_TEMPLATE['audio']
        )
        container = []
        for obj in data['audio']:
            container.append(
                template.render(
                    **obj
                )
            )
        info_data['prerender_audio'] = ', '.join(container)


        # Text streams
        template = environment.from_string(
            EBU_CORE_TEMPLATE['text']
        )
        container = []
        for obj in data['text']:
            container.append(
                template.render(
                    **obj
                )
            )
        info_data['prerender_text'] = ', '.join(container)


        # Main template
        template = environment.from_string(
            EBU_CORE_TEMPLATE['main']
        )
        result = template.render(
            **info_data
        )
        
        #print('result:', result)
        
        return json.loads(result)





####################################################################################################
# Testing

if __name__ == '__main__':
    print('Testing TrpMediametaToEBUCore')
    print('==================')
    print('')
    m_info = trp_mediainfo.TrpMediainfo('/home/bx/Videos/Silicon Valley - Season 1-6 [2014]/Season 5/Silicon Valley (2014) - S05E06 - Artificial Emotional Intelligence.mkv')
    #print('data:', json.dumps(m_info.data))
    print('data:', m_info.data)
    print('')
    result = TrpMediametaToEBUCore.convert(m_info.data)
    print('ebucore_data:', result)
    print('')


