#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys

# Try to find the utils directory and add it to the search path.
def add_utils_search_path(dir: str) -> bool:
    for p in sys.path:
        if p.endswith('/utils'):
            return True
    while len(dir.split(os.sep)) > 1:
        dir = os.path.dirname(dir)
        t_dir = os.path.join(dir, 'utils')
        if os.path.isdir(t_dir):
            sys.path.insert(0, t_dir)
            return True
    return False
self_dir = os.path.dirname(os.path.realpath(__file__))
if not os.path.isfile(os.path.join(self_dir, 'utils.py')):
    add_utils_search_path(self_dir)

import json
import subprocess

from utils import (
    to_bool,
    to_int,
    to_float,
    is_empty,
    to_list,
    ratio_to_number,
    to_surround_type,
    to_duration
)


# Transformers
STREAM_PROPERTY_TRANSFORMERS = [
	('index', 'index', lambda val: val),
	
	#('language', 'language', lambda val: val),
	
	('codec_name', 'codec_name', lambda val: val),
	('codec_long_name', 'codec_long_name', lambda val: val),
	('codec_tag_string', 'codec_tag', lambda val: val),
	('profile', 'profile', lambda val: val),
	
	('width', 'width', lambda val: val),
	('height', 'height', lambda val: val),
	('coded_width', 'coded_width', lambda val: val),
	('coded_height', 'coded_height', lambda val: val),
	
	('sample_aspect_ratio', 'sample_aspect_ratio', lambda val: val),
	('display_aspect_ratio', 'display_aspect_ratio', lambda val: val),
	('sample_aspect_ratio', 'sample_aspect_ratio_num', lambda val: ratio_to_number(val)),
	('display_aspect_ratio', 'display_aspect_ratio_num', lambda val: ratio_to_number(val)),
	
	('pix_fmt', 'pix_format', lambda val: val),
	('is_avc', 'is_avc', lambda val: to_bool(val)),
	
	('has_b_frames', 'has_b_frames', lambda val: to_bool(val)),
	('nb_frames', 'num_frames', lambda val: to_int(val)),
 
	('sample_fmt', 'sample_format', lambda val: val),
	('sample_rate', 'sample_rate', lambda val: to_int(val)),
	('channels', 'channels', lambda val: to_int(val)),
	#('channel_layout', 'channel_layout', lambda val: val),
	('bits_per_sample', 'bits_per_sample', lambda val: to_int(val)),
	
	('avg_frame_rate', 'frame_rate', lambda val: ratio_to_number(val)),
	('r_frame_rate', 'real_frame_rate', lambda val: ratio_to_number(val)),
	('time_base', 'time_base', lambda val: ratio_to_number(val)),
	('start_time', 'start_time', lambda val: to_float(val)),
	('duration', 'duration', lambda val: to_float(val)),
	('bit_rate', 'bit_rate', lambda val: to_int(val)),
	('bits_per_raw_sample', 'bits_per_raw_sample', lambda val: to_int(val))
]

DISPOSITION_PROPERTY_TRANSFORMERS = [
	('default', 'default', lambda val: to_bool(val)),
	('dub', 'dub', lambda val: to_bool(val)),
	('original', 'original', lambda val: to_bool(val)),
	('comment', 'comment', lambda val: to_bool(val)),
	('lyrics', 'lyrics', lambda val: to_bool(val)),
	('karaoke', 'karaoke', lambda val: to_bool(val)),
	('forced', 'forced', lambda val: to_bool(val)),
	('hearing_impaired', 'hearing_impaired', lambda val: to_bool(val)),
	('visual_impaired', 'visual_impaired', lambda val: to_bool(val)),
	('clean_effects', 'clean_effects', lambda val: to_bool(val)),
	('attached_pic', 'attached_pic', lambda val: to_bool(val)),
	('timed_thumbnails', 'timed_thumbnails', lambda val: to_bool(val))
]

INFO_PROPERTY_TRANSFORMERS = [
	('filename', 'filename', lambda val: val),
	('nb_streams', 'num_streams', lambda val: to_int(val)),
	('nb_programs', 'num_programs', lambda val: to_int(val)),
	('format_name', 'format_name', lambda val: to_list(val)),
	('format_long_name', 'format_long_name', lambda val: val),
	('start_time', 'start_time', lambda val: to_float(val)),
	('duration', 'duration', lambda val: to_float(val)),
	('size', 'size', lambda val: to_int(val)),
	('bit_rate', 'bit_rate', lambda val: to_int(val)),
	('tags', 'tags', lambda val: val)
]


# Templates
INFO_PROPERTY_TEMPLATE = {
    'title': None,
    'filename': None,
    'num_streams': None,
    'num_programs': None,
    'format_name': None,
    'format_long_name': None,
    'start_time': None,
    'duration': None,
    'size': None,
    'bit_rate': None,
}

AUDIO_PROPERTY_TEMPLATE = {
    'index': None,
    
    'language': 'und',
    
    'codec_name': None,
    'codec_long_name': None,
    'codec_tag': None,
    'profile': None,
    
    'sample_format': None,
    'sample_rate': None,
    'bits_per_sample': None,
    
    'channels': None,
    'channel_layout': None,
    
    'time_base': None,
    'start_time': 0,
    'duration': None,
    'bit_rate': None,
    'bits_per_raw_sample': None
}

VIDEO_PROPERTY_TEMPLATE = {
    'index': None,
    
    'language': 'und',
    
    'codec_name': None,
    'codec_long_name': None,
    'codec_tag': None,
    'profile': None,
    
    'width': None,
    'height': None,
    
    'coded_width': None,
    'coded_height': None,
    
    'sample_aspect_ratio': None,
    'display_aspect_ratio': None,
    
    'sample_aspect_ratio_num': None,
    'display_aspect_ratio_num': None,
    
    'pix_format': None,
    'is_avc': None,
    'has_b_frames': False,
    'num_frames': None,
    'frame_rate': None,
    'time_base': None,
    'start_time': None,
    'duration': None,
    'bit_rate': None
}

DISPOSITION_PROPERTY_TEMPLATE = {
    'default': False,
    'dub': False,
    'original': False,
    'comment': False,
    'lyrics': False,
    'karaoke': False,
    'forced': False,
    'hearing_impaired': False,
    'visual_impaired': False,
    'clean_effects': False,
    'attached_pic': False,
    'timed_thumbnails': False
}


# Maps
MAP_CHANNEL_TO_LAYOUT = {
    1: 'mono',
    2: 'stereo',
    3: '3.0',
    4: '4.0',
    5: '5.0',
    6: '5.1',
    7: '7.0',
    8: '7.1',
    16: 'hexadecagonal',
    24: '22.2'
}



class TrpFFprobe_meta(type):
    def __init__(cls, *args, **kwargs):
        cls._ffprobe_bin = '/usr/bin/ffprobe'
    
    @property
    def ffprobe_bin(cls) -> str:
        return cls._ffprobe_bin
    
    @ffprobe_bin.setter
    def ffprobe_bin(cls, path: str) -> None:
        if not isinstance(path, str):
            raise ValueError('Path must be string')
        if not os.path.isfile(path):
            raise ValueError('Path must be file')
        cls._ffprobe_bin = path


class TrpFFprobe(metaclass=TrpFFprobe_meta):
    __slots__ = [
        '_json_data',
        '_has_streams',
        '_has_audio',
        '_has_video',
        '_has_data',
        '_has_info',
        '_audio_streams',
        '_video_streams',
        '_info',
        '_default_audio_stream',
        '_default_video_stream'
    ]
    
    def __init__(self, path: str | None = None):
        if not isinstance(path, str):
            raise ValueError('Path must be string')
        if not os.path.isfile(path):
            raise ValueError('Path must be file')
        
        data = subprocess.check_output([
            self._ffprobe_bin,
            '-v',
            'quiet',
            '-print_format',
            'json',
            '-show_format',
            '-show_streams',
            path
        ], text=True)
        
        print('TrpFFprobe data (raw):', data)
        
        data = json.loads(data)
        self._json_data = data
        
        #print('TrpFFprobe data (parsed):', data)
        
        self._has_streams = False
        self._has_info = False
        self._has_audio = False
        self._has_video = False
        self._has_data = False
        
        self._audio_streams = []
        self._video_streams = []
        self._default_audio_stream = None
        self._default_video_stream = None
        self._info = INFO_PROPERTY_TEMPLATE.copy()
        self._info['tags'] = {}
        
        if 'streams' in data and len(data['streams']) > 0:
            self._has_streams = True
        
        if 'format' in data:
            self._has_info = True
        
        if self._has_streams:
            for stream in data['streams']:
                if 'codec_type' in stream:
                    if stream['codec_type'] == 'audio':
                        self._has_audio = True
                        obj = AUDIO_PROPERTY_TEMPLATE.copy()
                        obj['disposition'] = DISPOSITION_PROPERTY_TEMPLATE.copy()
                        for k_src, k_trg, t in STREAM_PROPERTY_TRANSFORMERS:
                            if k_src in stream:
                                obj[k_trg] = t(stream[k_src])
                        if 'disposition' in stream:
                            s_obj = stream['disposition']
                            t_obj = obj['disposition']
                            for k_src, k_trg, t in DISPOSITION_PROPERTY_TRANSFORMERS:
                                if k_src in s_obj:
                                    t_obj[k_trg] = t(s_obj[k_src])
                        
                        # Set channel layout
                        if 'channel_layout' in stream:
                            obj['channel_layout'] = to_surround_type(
                                stream['channel_layout'],
                                obj['channels']
                            )
                        if not obj['channel_layout']:
                            obj['channel_layout'] = MAP_CHANNEL_TO_LAYOUT[obj['channels']]
                        
                        # Set tags
                        if 'tags' in stream:
                            if (
                                'language' in stream['tags'] and
                                not is_empty(stream['tags']['language']) and
                                stream['tags']['language'] != 'und'
                            ):
                                obj['language'] = stream['tags']['language']
                        
                        # Add to audio streams
                        self._audio_streams.append(obj)
                        
                        if (
                            obj['disposition']['attached_pic'] == 0 and
                            obj['time_base'] > 0
                        ):
                            if (
                                self._default_audio_stream is None or
                                obj['disposition']['default'] > 0
                            ):
                                self._default_audio_stream = obj
                    
                    elif stream['codec_type'] == 'video':
                        self._has_video = True
                        obj = VIDEO_PROPERTY_TEMPLATE.copy()
                        obj['disposition'] = DISPOSITION_PROPERTY_TEMPLATE.copy()
                        for k_src, k_trg, t in STREAM_PROPERTY_TRANSFORMERS:
                            if k_src in stream:
                                obj[k_trg] = t(stream[k_src])
                        if 'disposition' in stream:
                            s_obj = stream['disposition']
                            t_obj = obj['disposition']
                            for k_src, k_trg, t in DISPOSITION_PROPERTY_TRANSFORMERS:
                                if k_src in s_obj:
                                    t_obj[k_trg] = t(s_obj[k_src])
                        
                        # Set tags
                        if 'tags' in stream:
                            if (
                                'language' in stream['tags'] and
                                not is_empty(stream['tags']['language']) and
                                stream['tags']['language'] != 'und'
                            ):
                                obj['language'] = stream['tags']['language']
                        
                        self._video_streams.append(obj)
                        
                        if (
                            obj['disposition']['attached_pic'] == 0 and
                            obj['frame_rate'] > 0
                        ):
                            if (
                                self._default_video_stream is None or
                                obj['disposition']['default'] > 0
                            ):
                                self._default_video_stream = obj
        
        
        if self._has_info:
            s_obj = data['format']
            for k_src, k_trg, t in INFO_PROPERTY_TRANSFORMERS:
                if k_src in s_obj:
                    self._info[k_trg] = t(s_obj[k_src])
            
            # Set tags
            if 'tags' in s_obj:
                if (
                    'title' in s_obj['tags'] and
                    isinstance(s_obj['tags']['title'], str)
                ):
                    self._info['title'] = s_obj['tags']['title']
                else:
                    f_name = os.path.basename(self._info['filename'])
                    f_name, f_ext = os.path.splitext(f_name)
                    self._info['title'] = f_name
    
    
    
    @property
    def json_data(self) -> dict:
        """Get JSON data"""
        return self._json_data
    
    @property
    def data(self) -> dict:
        """Get formated/parsed data"""
        return {
            'hasInfo': self._has_info,
            'hasStreams': self._has_streams,
            'hasAudio': self._has_audio,
            'hasVideo': self._has_video,
            'hasText': self._has_text,
            'hasMenu': self._has_menu,
            
            'info': self._info,
            'audio': self._audio_streams,
            'video': self._video_streams,
            'text': self._text_streams,
            'menu': self._menu_streams,
            
            'default': {
                'audio': self._default_audio_stream,
                'video': self._default_video_stream,
                'text': self._default_text_stream,
                'menu': self._default_menu_stream
            }
        }
    
    
    @property
    def has_streams(self) -> bool:
        """Get has_streams"""
        return self._has_streams
    
    @property
    def has_audio(self) -> bool:
        """Get has_audio"""
        return self._has_audio
    
    @property
    def has_video(self) -> bool:
        """Get has_video"""
        return self._has_video
    
    @property
    def has_data(self) -> bool:
        """Get has_data"""
        return self._has_data
    
    @property
    def has_info(self) -> bool:
        """Get has_info"""
        return self._has_info
    
    @property
    def info(self) -> dict:
        """Get info"""
        return self._info
    
    @property
    def audio_streams(self) -> list:
        """Get audio streams"""
        return self._audio_streams
    
    @property
    def video_streams(self) -> list:
        """Get video streams"""
        return self._video_streams
    
    
    
    @property
    def title(self) -> str | None:
        """Get title"""
        return self._info['title']
    
    @property
    def filename(self) -> str | None:
        """Get filename"""
        return self._info['filename']
    
    @property
    def num_streams(self) -> str | None:
        """Get num streams"""
        return self._info['num_streams']
    
    @property
    def num_programs(self) -> str | None:
        """Get num programs"""
        return self._info['num_programs']
    
    @property
    def start_time(self) -> float | None:
        """Get start time"""
        return self._info['start_time']
    
    @property
    def start_time_ts(self) -> str | None:
        """Get start time timestamp"""
        if self._info['start_time'] is None:
            return None
        return to_duration(self._info['start_time'] * 1000)
    
    @property
    def duration(self) -> float | None:
        """Get duration"""
        return self._info['duration']
    
    @property
    def duration_ts(self) -> str | None:
        """Get duration timestamp"""
        if self._info['duration'] is None:
            return None
        return to_duration(self._info['duration'] * 1000)
    
    @property
    def size(self) -> int | None:
        """Get size"""
        return self._info['size']
    
    @property
    def bit_rate(self) -> int | None:
        """Get bit rate"""
        return self._info['bit_rate']
    
    @property
    def format_name(self) -> int | None:
        """Get format name"""
        return self._info['format_name']
    
    @property
    def format_long_name(self) -> int | None:
        """Get format long name"""
        return self._info['format_long_name']
    
    
    
    @property
    def default_audio_stream(self) -> dict | None:
        """Get default audio stream"""
        return self._default_audio_stream
    
    @property
    def default_video_stream(self) -> dict | None:
        """Get default video stream"""
        return self._default_video_stream
    
    
    
    @property
    def default_language(self) -> str:
        """Get default language"""
        val = self._default_audio_stream['language']
        if self._default_video_stream and val == 'und':
            return self._default_video_stream['language']
        return val
    
    def language(self, index: int) -> str:
        """Get language"""
        num_audio = len(self._audio_streams)
        if index < 0 or index >= num_audio:
            raise ValueError('Invalid audio stream index')
        val = self._audio_streams[index]['language']
        if val == 'und':
            return self.default_language
        return val



####################################################################################################
# Testing

if __name__ == '__main__':
    print('Testing TrpFFprobe')
    print('==================')
    print('')
    print('ffprobe_bin:', TrpFFprobe.ffprobe_bin)
    ffp = TrpFFprobe()
    print('ffprobe:', ffp)
