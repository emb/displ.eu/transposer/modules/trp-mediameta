#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import sys
import typing

# Try to find the utils directory and add it to the search path.
def add_utils_search_path(dir: str) -> bool:
    for p in sys.path:
        if p.endswith('/utils'):
            return True
    while len(dir.split(os.sep)) > 1:
        dir = os.path.dirname(dir)
        t_dir = os.path.join(dir, 'utils')
        if os.path.isdir(t_dir):
            sys.path.insert(0, t_dir)
            return True
    return False
self_dir = os.path.dirname(os.path.realpath(__file__))
if not os.path.isfile(os.path.join(self_dir, 'utils.py')):
    add_utils_search_path(self_dir)

import json
import subprocess

from utils import (
    to_bool,
    to_int,
    to_float,
    to_list,
    to_duration,
    to_datetime,
    get_numerator,
    get_denominator
)





# Transformers
INFO_STREAM_PROPERTY_TRANSFORMERS = [
    ('UniqueID', 'uuid', lambda val: val),
    
    ('StreamCount', 'streamCount', lambda val: to_int(val)),
    ('VideoCount', 'videoCount', lambda val: to_int(val)),
    ('AudioCount', 'audioCount', lambda val: to_int(val)),
    ('TextCount', 'textCount', lambda val: to_int(val)),
    ('MenuCount', 'menuCount', lambda val: to_int(val)),

    ('Video_Format_List', 'videoFormat', lambda val: to_list(val, None, '/')),
    ('Video_Codec_List', 'videoCodec', lambda val: to_list(val, None, '/')),
    ('Video_Language_List', 'videoLanguage', lambda val: to_list(val, None, '/')),

    ('Audio_Format_List', 'audioFormat', lambda val: to_list(val, None, '/')),
    ('Audio_Codec_List', 'audioCodec', lambda val: to_list(val, None, '/')),
    ('Audio_Language_List', 'audioLanguage', lambda val: to_list(val, None, '/')),

    ('Text_Format_List', 'textFormat', lambda val: to_list(val, None, '/')),
    ('Text_Codec_List', 'textCodec', lambda val: to_list(val, None, '/')),
    ('Text_Language_List', 'textLanguage', lambda val: to_list(val, None, '/')),

    ('CompleteName', 'filePath', lambda val: val),
    ('FolderName', 'folderName', lambda val: val),
    #('FileNameExtension', 'fileNameExtension', lambda val: val),
    ('FileName', 'fileName', lambda val: val),
    ('FileExtension', 'fileExtension', lambda val: val),

    ('Format', 'format', lambda val: val),
    ('Format_Extensions', 'formatExtensions', lambda val: to_list(val, None, ' ')),
    ('Format_Profile', 'formatProfile', lambda val: val),
    ('InternetMediaType', 'internetMediaType', lambda val: val),
    ('CodecID', 'codecID', lambda val: val),
    ('CodecID_Url', 'codecIDUrl', lambda val: val),
    ('CodecID_Compatible', 'codecIDCompatible', lambda val: val),

    ('FileSize', 'fileSize', lambda val: to_int(val)),
    ('FileSize_String', 'fileSizeFormated', lambda val: val),

    ('Duration', 'duration', lambda val: to_float(val)),
    ('Duration_String3', 'durationFormated', lambda val: val),
    ('Duration_String4', 'durationSMTP', lambda val: val),

    ('OverallBitRate_Mode', 'overallBitRateMode', lambda val: val),
    ('OverallBitRate', 'overallBitRate', lambda val: to_int(val)),

    ('FrameRate', 'frameRate', lambda val: to_float(val)),
    ('FrameCount', 'frameCount', lambda val: to_int(val)),
    ('StreamSize', 'streamSize', lambda val: to_int(val)),
    ('HeaderSize', 'headerSize', lambda val: to_int(val)),
    ('DataSize', 'dataSize', lambda val: to_int(val)),
    ('FooterSize', 'footerSize', lambda val: to_int(val)),
    ('IsStreamable', 'isStreamable', lambda val: to_bool(val)),

    ('Title', 'title', lambda val: val),
    ('Movie', 'movie', lambda val: val),
    ('Performer', 'performer', lambda val: val),
    ('Genre', 'genre', lambda val: val),
    
    ('Encoded_Date', 'encodedDate', lambda val: to_datetime(val)),
    ('Tagged_Date', 'taggedDate', lambda val: to_datetime(val)),
    ('File_Modified_Date', 'fileModifiedDate', lambda val: to_datetime(val))
]
EBUCORE_INFO_STREAM_PROPERTY_TRANSFORMERS = {
    'ebucore:format__ebucore:containerFormat__ebucore:containerEncoding__@formatLabel': [
        ('info', 'containerEncoding', lambda val: val)
    ],
    'ebucore:format__ebucore:containerFormat__@containerFormatName': [
        ('info', 'containerFormatName', lambda val: val)
    ],
    'ebucore:format__ebucore:containerFormat__@containerFormatVersionId': [
        ('info', 'containerFormatVersionId', lambda val: val)
    ],
    'ebucore:format__ebucore:containerFormat__@containerFormatId': [
        ('info', 'containerFormatId', lambda val: val)
    ],
    
    'ebucore:format__ebucore:containerFormat__ebucore:technicalAttributeString__@typeLabel:WritingApplication__#value': [
        ('info', 'containerWritingApplication', lambda val: val)
    ],
    'ebucore:format__ebucore:containerFormat__ebucore:technicalAttributeString__@typeLabel:WritingLibrary__#value': [
        ('info', 'containerWritingLibrary', lambda val: val)
    ],
    
    'ebucore:format__ebucore:duration__ebucore:normalPlayTime__#value': [
        ('info', 'durationEBUCore', lambda val: val)
    ],
    
    'ebucore:format__ebucore:fileSize__#value': [
        ('info', 'fileSize', lambda val: to_int(val))
    ],
    'ebucore:format__ebucore:fileName__#value': [
        ('info', 'fileName', lambda val: val)
    ],
    'ebucore:format__ebucore:locator__#value': [
        ('info', 'filePath', lambda val: val)
    ],
    
    'ebucore:format__ebucore:technicalAttributeInteger__@typeLabel:OverallBitRate__@unit': [
        ('info', 'overallBitRateUnit', lambda val: val)
    ],
    'ebucore:format__ebucore:technicalAttributeInteger__@typeLabel:OverallBitRate__#value': [
        ('info', 'overallBitRate', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:dateCreated__@startDate': [
        ('info', 'dateCreatedDate', lambda val: val)
    ],
    'ebucore:format__ebucore:dateCreated__@startTime': [
        ('info', 'dateCreatedTime', lambda val: val)
    ]
}

COMMON_STREAM_PROPERTY_TRANSFORMERS = [
    ('UniqueID', 'uuid', lambda val: val),
    ('ID', 'id', lambda val: to_int(val)),
    ('StreamCount', 'streamCount', lambda val: to_int(val)),
    ('StreamOrder', 'streamOrder', lambda val: to_int(val)),
    ('@typeorder', 'typeOrder', lambda val: to_int(val))
]

COMMON_MEDIA_STREAM_PROPERTY_TRANSFORMERS = [
    ('Format', 'format', lambda val: val),
    ('CodecID', 'codecID', lambda val: val),
    
    ('Duration', 'duration', lambda val: to_float(val)),
    ('Duration_String3', 'durationFormated', lambda val: val),
    
    ('BitRate', 'bitRate', lambda val: to_int(val)),
    
    ('FrameRate', 'frameRate', lambda val: to_float(val)),
    ('FrameCount', 'frameCount', lambda val: to_int(val)),
    
    ('StreamSize', 'streamSize', lambda val: to_int(val)),
    
    ('Language', 'languageCode', lambda val: val),
    ('Language_String3', 'languageCode3', lambda val: val),
    ('Language_String', 'language', lambda val: val),
    
    ('Default', 'default', lambda val: to_bool(val)),
    ('Forced', 'forced', lambda val: to_bool(val))
]

VIDEO_STREAM_PROPERTY_TRANSFORMERS = [
    ('Format_Commercial', 'codecIDName', lambda val: val),
    
    ('Format_Url', 'formatUrl', lambda val: val),
    ('Format_Profile', 'formatProfile', lambda val: val),
    ('Format_Level', 'formatLevel', lambda val: val),
    ('Format_Tier', 'formatTier', lambda val: val),

    ('Format_Settings', 'formatSettings', lambda val: val),
    ('Format_Settings_CABAC', 'formatSettingsCABAC', lambda val: to_bool(val)),
    ('Format_Settings_RefFrames', 'formatSettingsRefFrames', lambda val: to_int(val)),
    ('Format_Settings_GOP', 'formatSettingsGOP', lambda val: val),
    
    ('InternetMediaType', 'internetMediaType', lambda val: val),
    
    ('FileName', 'fileName', lambda val: val),
    ('FileExtension', 'fileExtension', lambda val: val),

    ('Duration_String4', 'durationSMTP', lambda val: val),
    
    ('Delay', 'delay', lambda val: to_float(val)),
    ('Delay_String3', 'delayFormated', lambda val: val),
    ('Delay_Source', 'delaySource', lambda val: val),

    ('Width', 'width', lambda val: to_int(val)),
    ('Height', 'height', lambda val: to_int(val)),
    ('Stored_Height', 'storedHeight', lambda val: to_int(val)),
    ('Sampled_Width', 'sampledWidth', lambda val: to_int(val)),
    ('Sampled_Height', 'sampledHeight', lambda val: to_int(val)),

    ('PixelAspectRatio', 'pixelAspectRatio', lambda val: to_float(val)),
    ('DisplayAspectRatio', 'displayAspectRatio', lambda val: to_float(val)),
    ('DisplayAspectRatio_String', 'displayAspectRatioString', lambda val: val),
    ('DisplayAspectRatio_String', 'displayAspectRatioNum', lambda val: get_numerator(val)),
    ('DisplayAspectRatio_String', 'displayAspectRatioDen', lambda val: get_denominator(val)),

    ('Rotation', 'rotation', lambda val: to_float(val)),
    
    ('FrameRate_Mode', 'frameRateMode', lambda val: val),
    ('FrameRate_Num', 'frameRateBase', lambda val: int(to_float(val) * 0.001)),
    ('FrameRate_Num', 'frameRateNum', lambda val: 1000),
    ('FrameRate_Den', 'frameRateDen', lambda val: to_int(val)),

    ('ColorSpace', 'colorSpace', lambda val: val),
    ('ChromaSubsampling', 'chromaSubsampling', lambda val: val),
    ('BitDepth', 'bitDepth', lambda val: to_int(val)),
    ('ScanType', 'scanType', lambda val: val),

    ('colour_description_present', 'colorDescriptionPresent', lambda val: to_bool(val)),
    ('colour_range', 'colorRange', lambda val: val),
    ('colour_primaries', 'colorPrimaries', lambda val: val),
    ('transfer_characteristics', 'transferCharacteristics', lambda val: val),
    ('matrix_coefficients', 'matrixCoefficients', lambda val: val),

    ('Encoded_Library_Name', 'encodedLibraryName', lambda val: val),
    ('Encoded_Library_Version', 'encodedLibraryVersion', lambda val: val),
    #('Encoded_Library_Settings', 'encodedLibrarySettings', lambda val: val),
    
    ('Encoded_Date', 'encodedDate', lambda val: to_datetime(val)),
    ('Tagged_Date', 'taggedDate', lambda val: to_datetime(val))
]
EBUCORE_VIDEO_STREAM_PROPERTY_TRANSFORMERS = {
    'ebucore:format__ebucore:videoFormat': [
        ('video', 'create_container', lambda val: {})
    ],
    'ebucore:format__ebucore:videoFormat__@videoFormatName': [
        ('video', 'format', lambda val: val)
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:width__@unit': [
        ('video', 'widthUnit', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:width__#value': [
        ('video', 'width', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:height__@unit': [
        ('video', 'heightUnit', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:height__#value': [
        ('video', 'height', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:frameRate__@factorNumerator': [
        ('video', 'frameRateNum', lambda val: to_int(val))
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:frameRate__@factorDenominator': [
        ('video', 'frameRateDen', lambda val: to_int(val))
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:frameRate__#value': [
        ('video', 'frameRateBase', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:aspectRatio__@typeLabel': [
        ('video', 'aspectRatioType', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:aspectRatio__@typeLabel:display__ebucore:factorNumerator__#value': [
        ('video', 'aspectRatioNum', lambda val: to_int(val))
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:aspectRatio__@typeLabel:display__ebucore:factorDenominator__#value': [
        ('video', 'aspectRatioDen', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:videoEncoding__@typeLabel': [
        ('video', 'encoding', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:codec__ebucore:codecIdentifier__dc:identifier__#value': [
        ('video', 'codecID', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:codec__ebucore:name__#value': [
        ('video', 'codecIDName', lambda val: val)
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:bitRate__#value': [
        ('video', 'bitRate', lambda val: to_int(val))
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:videoTrack__@trackId': [
        ('video', 'id', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:ColorSpace__#value': [
        ('video', 'colorSpace', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:ChromaSubsampling__#value': [
        ('video', 'chromaSubsampling', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:colour_primaries__#value': [
        ('video', 'colorPrimaries', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:transfer_characteristics__#value': [
        ('video', 'transferCharacteristics', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:matrix_coefficients__#value': [
        ('video', 'matrixCoefficients', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:colour_range__#value': [
        ('video', 'colorRange', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:WritingLibrary__#value': [
        ('video', 'encodedLibraryName', lambda val: val.split(' ', 1)[0]),
        ('video', 'encodedLibraryVersion', lambda val: val.split(' ', 1)[1] if len(val.split(' ', 1)) > 1 else None)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:Default__#value': [
        ('video', 'default', lambda val: to_bool(val))
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeString__@typeLabel:Forced__#value': [
        ('video', 'forced', lambda val: to_bool(val))
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeInteger__@typeLabel:BitDepth__@unit': [
        ('video', 'bitDepthUnit', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeInteger__@typeLabel:BitDepth__#value': [
        ('video', 'bitDepth', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeInteger__@typeLabel:StreamSize__@unit': [
        ('video', 'streamSizeUnit', lambda val: val)
    ],
    'ebucore:format__ebucore:videoFormat__ebucore:technicalAttributeInteger__@typeLabel:StreamSize__#value': [
        ('video', 'streamSize', lambda val: to_int(val))
    ]
}

AUDIO_STREAM_PROPERTY_TRANSFORMERS = [
    ('Format_Commercial', 'codecIDName', lambda val: val),
    
    ('Format_Url', 'formatUrl', lambda val: val),
    ('Format_AdditionalFeatures', 'formatAdditionalFeatures', lambda val: val),
    ('Format_Settings_Endianness', 'formatSettingsEndianness', lambda val: val),

    ('Duration_String4', 'durationSMTP', lambda val: val),
    
    ('Source_Duration', 'sourceDuration', lambda val: to_float(val)),
    ('Source_Duration_String3', 'sourceDurationFormated', lambda val: val),
    
    ('Delay', 'delay', lambda val: to_float(val)),
    ('Delay_String3', 'delayFormated', lambda val: val),
    ('Delay_Source', 'delaySource', lambda val: val),
    
    ('Video_Delay', 'videoDelay', lambda val: to_float(val)),
    ('Video_Delay_String3', 'videoDelayFormated', lambda val: val),

    ('BitRate_Mode', 'bitRateMode', lambda val: val),
    ('BitRate_Maximum', 'bitRateMaximum', lambda val: to_int(val)),

    ('Channels', 'channels', lambda val: to_int(val)),
    ('ChannelPositions', 'channelPositions', lambda val: val),
    ('ChannelPositions_String2', 'channelPositionsFormated', lambda val: val),
    ('ChannelLayout', 'channelLayout', lambda val: val),
    
    ('SamplesPerFrame', 'samplesPerFrame', lambda val: to_int(val)),
    ('SamplingRate', 'samplingRate', lambda val: to_int(val)),
    ('SamplingCount', 'samplingCount', lambda val: to_int(val)),
    
    ('Source_FrameCount', 'sourceFrameCount', lambda val: to_int(val)),
    ('Compression_Mode', 'compressionMode', lambda val: val),
    ('Source_StreamSize', 'sourceStreamSize', lambda val: to_int(val)),
    ('ServiceKind', 'serviceKind', lambda val: val),
    
    ('Encoded_Date', 'encodedDate', lambda val: to_datetime(val)),
    ('Tagged_Date', 'taggedDate', lambda val: to_datetime(val))
]
EBUCORE_AUDIO_STREAM_PROPERTY_TRANSFORMERS = {
    'ebucore:format__ebucore:audioFormat': [
        ('audio', 'create_container', lambda val: {})
    ],
    'ebucore:format__ebucore:audioFormat__@audioFormatName': [
        ('audio', 'format', lambda val: val)
    ],
    
    'ebucore:format__ebucore:audioFormat__ebucore:audioEncoding__@typeLabel': [
        ('audio', 'encoding', lambda val: val)
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:audioEncoding__@typeLink': [
        ('audio', 'encodingUrl', lambda val: val)
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:codec__ebucore:codecIdentifier__dc:identifier__#value': [
        ('audio', 'codecID', lambda val: val)
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:codec__ebucore:name__#value': [
        ('audio', 'codecIDName', lambda val: val)
    ],
    
    'ebucore:format__ebucore:audioFormat__ebucore:samplingRate__#value': [
        ('audio', 'samplingRate', lambda val: to_int(val))
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:bitRate__#value': [
        ('audio', 'bitRate', lambda val: to_int(val))
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:bitRateMode__#value': [
        ('audio', 'bitRateMode', lambda val: 'CBR' if val.lower() == 'constant' else 'VBR')
    ],
    
    'ebucore:format__ebucore:audioFormat__ebucore:audioTrack__@trackId': [
        ('audio', 'id', lambda val: to_int(val))
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:audioTrack__@trackLanguage': [
        ('audio', 'languageCode', lambda val: val)
    ],
    
    'ebucore:format__ebucore:audioFormat__ebucore:channels__#value': [
        ('audio', 'channels', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:audioFormat__ebucore:technicalAttributeString__@typeLabel:ChannelPositions__#value': [
        ('audio', 'channelPositions', lambda val: val)
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:technicalAttributeString__@typeLabel:ChannelLayout__#value': [
        ('audio', 'channelLayout', lambda val: val)
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:technicalAttributeString__@typeLabel:Endianness__#value': [
        ('audio', 'formatSettingsEndianness', lambda val: val)
    ],
    
    'ebucore:format__ebucore:audioFormat__ebucore:technicalAttributeInteger__@typeLabel:StreamSize__@unit': [
        ('audio', 'streamSizeUnit', lambda val: val)
    ],
    'ebucore:format__ebucore:audioFormat__ebucore:technicalAttributeInteger__@typeLabel:StreamSize__#value': [
        ('audio', 'streamSize', lambda val: to_int(val))
    ]
}

TEXT_STREAM_PROPERTY_TRANSFORMERS = [
    ('CodecID_Info', 'codecIDName', lambda val: val),
    ('ElementCount', 'elementCount', lambda val: to_int(val)),
    ('Title', 'title', lambda val: val)
]
EBUCORE_TEXT_STREAM_PROPERTY_TRANSFORMERS = {
    'ebucore:format__ebucore:dataFormat': [
        ('text', 'create_container', lambda val: {})
    ],
    'ebucore:format__ebucore:dataFormat__@dataFormatName': [
        ('text', 'format', lambda val: val)
    ],
    'ebucore:format__ebucore:dataFormat__@dataTrackId': [
        ('text', 'id', lambda val: to_int(val))
    ],
    
    'ebucore:format__ebucore:dataFormat__ebucore:captioningFormat__@language': [
        ('text', 'languageCode', lambda val: val)
    ],
    'ebucore:format__ebucore:dataFormat__ebucore:captioningFormat__@typeLabel': [
        ('text', 'typeLabel', lambda val: val)
    ],
    
    'ebucore:format__ebucore:dataFormat__ebucore:codec__ebucore:codecIdentifier__dc:identifier__#value': [
        ('text', 'codecID', lambda val: val)
    ]
}

MENU_STREAM_PROPERTY_TRANSFORMERS = [
    ('Chapters_Pos_Begin', 'chaptersPosBegin', lambda val: to_int(val)),
    ('Chapters_Pos_End', 'chaptersPosEnd', lambda val: to_int(val))
]


# Templates
INFO_STREAM_TEMPLATE = {
    'uuid': None,
    
    'streamCount': None,
    'videoCount': 0,
    'audioCount': 0,
    'textCount': 0,
    'menuCount': 0,

    'videoFormat': None,
    'videoCodec': None,
    'videoLanguage': None,

    'audioFormat': None,
    'audioCodec': None,
    'audioLanguage': None,

    'textFormat': None,
    'textCodec': None,
    'textLanguage': None,

    'filePath': None,
    'folderName': None,
    #'fileNameExtension': None,
    'fileName': None,
    'fileExtension': None,

    'format': None,
    'formatExtensions': None,
    'formatProfile': None,
    'internetMediaType': None,
    'codecID': None,
    'codecIDUrl': None,
    'codecIDCompatible': None,

    'fileSize': 0,
    'fileSizeFormated': None,

    'duration': 0.0,
    'durationFormated': None,
    'durationSMTP': None,
    'durationEBUCore': None,

    'overallBitRateMode': None,
    'overallBitRate': None,
    'overallBitRateUnit': 'bps',

    'frameRate': 0.0,
    'frameCount': 0,
    'streamSize': 0,
    'headerSize': 0,
    'dataSize': 0,
    'footerSize': 0,
    'isStreamable': False,

    'title': None,
    'movie': None,
    'performer': None,
    'genre': None,
    
    'encodedDate': None,
    'taggedDate': None,
    'fileModifiedDate': None,
}

COMMON_STREAM_TEMPLATE = {
    'uuid': None,
    'id': None,
    'streamCount': None,
    'streamOrder': 0,
    'typeOrder': 0
}

COMMON_MEDIA_STREAM_TEMPLATE = {
    'format': None,
    'codecID': None,
    
    'duration': 0.0,
    'durationFormated': None,
    
    'bitRate': None,
    
    'frameRate': 0.0,
    'frameCount': 0,
    
    'streamSize': 0,
    'streamSizeUnit': 'byte',
    
    'languageCode': None,
    'languageCode3': None,
    'language': None,
    
    'default': False,
    'forced': False
}

VIDEO_STREAM_TEMPLATE = {
    'codecIDName': None,
    'encoding': None,
    'encodingUrl': None,
    
    'formatUrl': None,
    'formatProfile': None,
    'formatLevel': None,
    'formatTier': None,

    'formatSettings': None,
    'formatSettingsCABAC': False,
    'formatSettingsRefFrames': None,
    'formatSettingsGOP': None,
    
    'internetMediaType': None,
    
    'fileName': None,
    'fileExtension': None,

    'durationSMTP': None,
    'durationEBUCore': None,
    
    'delay': 0.0,
    'delayFormated': None,
    'delaySource': None,

    'width': None,
    'widthUnit': 'pixel',
    'height': None,
    'heightUnit': 'pixel',
    'storedHeight': None,
    'sampledWidth': None,
    'sampledHeight': None,

    'pixelAspectRatio': None,
    'displayAspectRatio': None,
    'displayAspectRatioString': None,
    'displayAspectRatioNum': None,
    'displayAspectRatioDen': None,

    'rotation': 0.0,
    
    'frameRateMode': None,
    'frameRateBase': None,
    'frameRateNum': None,
    'frameRateDen': None,
    
    'colorSpace': None,
    'chromaSubsampling': None,
    'bitDepth': 0,
    'bitDepthUnit': 'bit',
    'scanType': None,
    
    'colorDescriptionPresent': False,
    'colorRange': None,
    'colorPrimaries': None,
    'transferCharacteristics': None,
    'matrixCoefficients': None,

    'encodedLibraryName': None,
    'encodedLibraryVersion': None,
    #'encodedLibrarySettings': None,
    
    'encodedDate': None,
    'taggedDate': None
}

AUDIO_STREAM_TEMPLATE = {
    'codecIDName': None,
    'encoding': None,
    'encodingUrl': None,
    
    'formatUrl': None,
    'formatAdditionalFeatures': None,
    'formatSettingsEndianness': None,

    'durationSMTP': None,
    'durationEBUCore': None,
    
    'sourceDuration': 0.0,
    'sourceDurationFormated': None,
    
    'delay': 0.0,
    'delayFormated': None,
    'delaySource': None,
    
    'videoDelay': 0.0,
    'videoDelayFormated': None,

    'bitRateMode': None,
    'bitRateMaximum': 0,

    'channels': 0,
    'channelPositions': None,
    'channelPositionsFormated': None,
    'channelLayout': None,
    
    'samplesPerFrame': 0,
    'samplingRate': 0,
    'samplingCount': 0,
    
    'sourceFrameCount': 0,
    'compressionMode': None,
    'sourceStreamSize': 0,
    'serviceKind': None,
    
    'encodedDate': None,
    'taggedDate': None
}

TEXT_STREAM_TEMPLATE = {
    'codecIDName': None,
    'elementCount': 0,
    'title': None,
    'typeLabel': None
}

MENU_STREAM_TEMPLATE = {
    'chaptersPosBegin': 0,
    'chaptersPosEnd': 0,
    'extra': None
}






class TrpMediainfo_meta(type):
    def __init__(cls, *args, **kwargs):
        cls._mediainfo_bin = '/usr/bin/mediainfo'
    
    @property
    def mediainfo_bin(cls) -> str:
        return cls._mediainfo_bin
    
    @mediainfo_bin.setter
    def mediainfo_bin(cls, path: str) -> None:
        if not isinstance(path, str):
            raise ValueError('Path must be string')
        if not os.path.isfile(path):
            raise ValueError('Path must be file')
        cls._mediainfo_bin = path


class TrpMediainfo(metaclass=TrpMediainfo_meta):
    __slots__ = [
        '_json_data',
        '_ebucore_data',
        '_parsed_data',
        '_has_info',
        '_has_streams',
        '_has_audio',
        '_has_video',
        '_has_text',
        '_has_menu',
        '_info',
        '_audio_streams',
        '_video_streams',
        '_text_streams',
        '_menu_streams',
        '_default_audio_stream',
        '_default_video_stream',
        '_default_text_stream',
        '_default_menu_stream'
    ]
    
    def __init__(self, path: str | None = None):
        if not isinstance(path, str):
            raise ValueError('Path must be string')
        if not os.path.isfile(path):
            raise ValueError('Path must be file')
        
        data = subprocess.check_output([
            self._mediainfo_bin,
            '--Full',
            '--Output=JSON',
            path
        ], text=True)
        
        ebucore_data = subprocess.check_output([
            self._mediainfo_bin,
            '--Full',
            '--Output=EBUCore_JSON',
            path
        ], text=True)
        
        #print('TrpMediainfo data (raw):', data)
        
        data = json.loads(data)
        self._json_data = data
        
        ebucore_data = json.loads(ebucore_data)
        self._ebucore_data = ebucore_data
        
        #print('TrpMediainfo data (parsed):', data)
        
        self._has_info = False
        self._has_streams = False
        self._has_audio = False
        self._has_video = False
        self._has_text = False
        self._has_menu = False
        
        self._info = INFO_STREAM_TEMPLATE.copy()
        self._audio_streams = []
        self._video_streams = []
        self._text_streams = []
        self._menu_streams = []
        self._default_audio_stream = None
        self._default_video_stream = None
        self._default_text_stream = None
        self._default_menu_stream = None
        
        self._parsed_data = {
            'info': self._info,
            'audio': self._audio_streams,
            'video': self._video_streams,
            'text': self._text_streams,
            'menu': self._menu_streams
        }
        
        if (
            'media' not in data or
            'track' not in data['media'] or
            len(data['media']['track']) == 0
        ):
            return
        data = data['media']['track']
        
        video_pos = 0
        audio_pos = 0
        text_pos = 0
        menu_pos = 0
        for stream in data:
            if '@type' not in stream:
                continue
            
            if stream['@type'] == 'General':
                for k_src, k_trg, t in INFO_STREAM_PROPERTY_TRANSFORMERS:
                    if k_src in stream:
                        self._info[k_trg] = t(stream[k_src])
                self._has_info = True
                continue
            
            if stream['@type'] == 'Video':
                obj = {
                    **COMMON_STREAM_TEMPLATE,
                    **COMMON_MEDIA_STREAM_TEMPLATE,
                    **VIDEO_STREAM_TEMPLATE
                }
                for k_src, k_trg, t in [
                    *COMMON_STREAM_PROPERTY_TRANSFORMERS,
                    *COMMON_MEDIA_STREAM_PROPERTY_TRANSFORMERS,
                    *VIDEO_STREAM_PROPERTY_TRANSFORMERS
                ]:
                    if k_src in stream:
                        obj[k_trg] = t(stream[k_src])
                self._video_streams.append(obj)
                if not self._has_video or obj['default']:
                    self._default_video_stream = video_pos
                self._has_streams = True
                self._has_video = True
                video_pos += 1
                continue
            
            if stream['@type'] == 'Audio':
                obj = {
                    **COMMON_STREAM_TEMPLATE,
                    **COMMON_MEDIA_STREAM_TEMPLATE,
                    **AUDIO_STREAM_TEMPLATE
                }
                for k_src, k_trg, t in [
                    *COMMON_STREAM_PROPERTY_TRANSFORMERS,
                    *COMMON_MEDIA_STREAM_PROPERTY_TRANSFORMERS,
                    *AUDIO_STREAM_PROPERTY_TRANSFORMERS
                ]:
                    if k_src in stream:
                        obj[k_trg] = t(stream[k_src])
                self._audio_streams.append(obj)
                if not self._has_audio or obj['default']:
                    self._default_audio_stream = audio_pos
                self._has_streams = True
                self._has_audio = True
                audio_pos += 1
                continue
            
            if stream['@type'] == 'Text':
                obj = {
                    **COMMON_STREAM_TEMPLATE,
                    **COMMON_MEDIA_STREAM_TEMPLATE,
                    **TEXT_STREAM_TEMPLATE
                }
                for k_src, k_trg, t in [
                    *COMMON_STREAM_PROPERTY_TRANSFORMERS,
                    *COMMON_MEDIA_STREAM_PROPERTY_TRANSFORMERS,
                    *TEXT_STREAM_PROPERTY_TRANSFORMERS
                ]:
                    if k_src in stream:
                        obj[k_trg] = t(stream[k_src])
                self._text_streams.append(obj)
                if not self._has_text or obj['default']:
                    self._default_text_stream = text_pos
                self._has_streams = True
                self._has_text = True
                text_pos += 1
                continue
            
            if stream['@type'] == 'Menu':
                obj = {
                    **COMMON_STREAM_TEMPLATE,
                    **MENU_STREAM_TEMPLATE
                }
                for k_src, k_trg, t in [
                    *COMMON_STREAM_PROPERTY_TRANSFORMERS,
                    *MENU_STREAM_PROPERTY_TRANSFORMERS
                ]:
                    if k_src in stream:
                        obj[k_trg] = t(stream[k_src])
                if 'extra' in stream:
                    obj['extra'] = {}
                    for ts, val in stream['extra'].items():
                        ts = to_duration(ts)
                        if ts is None:
                            continue
                        obj['extra'][ts] = val
                self._menu_streams.append(obj)
                if not self._has_menu:
                    self._default_menu_stream = menu_pos
                self._has_streams = True
                self._has_menu = True
                menu_pos += 1
                continue
        
        # Parse EBUCore
        if 'ebucore:ebuCoreMain' not in ebucore_data:
            return
        ebucore_data = ebucore_data['ebucore:ebuCoreMain']
        if 'ebucore:coreMetadata' not in ebucore_data:
            return
        ebucore_data = ebucore_data['ebucore:coreMetadata']
        result_container = {
            'info': {},
            'audio': [],
            'video': [],
            'text': [],
            'menu': []
        }
        ebucore_transformers = {
            **EBUCORE_INFO_STREAM_PROPERTY_TRANSFORMERS,
            **EBUCORE_VIDEO_STREAM_PROPERTY_TRANSFORMERS,
            **EBUCORE_AUDIO_STREAM_PROPERTY_TRANSFORMERS,
            **EBUCORE_TEXT_STREAM_PROPERTY_TRANSFORMERS
        }
        
        self._parse_ebucore(ebucore_data, result_container, None, ebucore_transformers, [])
        
        # Update parsed data
        for typ, container in result_container.items():
            if typ not in self._parsed_data:
                continue
            if typ == 'info':
                self._parsed_data[typ].update(container)
                continue
            for obj in container:
                if 'id' not in obj:
                    continue
                if obj['id'] is None:
                    continue
                for stream in self._parsed_data[typ]:
                    if stream['id'] == obj['id']:
                        stream.update(obj)
                        break
        
        # Post processing
        if self._has_info:
            if self._info['streamCount'] is None:
                self._info['streamCount'] = (
                    len(self._audio_streams) + 
                    len(self._video_streams) + 
                    len(self._text_streams) + 
                    len(self._menu_streams)
                )
    
    
    
    def _parse_ebucore(self, data: typing.Any, container: dict | list, cache: dict | None, transformers: dict, path: list):
        # Create new cache container
        k = '__'.join(path)
        if k in transformers:
            if cache is None or len(cache.keys()) > 0:
                for typ, k_trg, t in transformers[k]:
                    if typ not in container:
                        continue
                    if k_trg != 'create_container':
                        continue
                    cache = t(data)
                    container[typ].append(cache)
                    break
        
        # Parse dict
        if isinstance(data, dict):
            for key, val in data.items():
                p = [*path, key]
                k = '__'.join(p)
                if (
                    key == '@typeLabel' and
                    k not in transformers
                ):
                    d = data.copy()
                    del d['@typeLabel']
                    self._parse_ebucore(d, container, cache, transformers, [*path, f'@typeLabel:{val}'])
                    return
                self._parse_ebucore(val, container, cache, transformers, p)
            return
        
        # Parse list
        if isinstance(data, list):
            for val in data:
                self._parse_ebucore(val, container, cache, transformers, path)
            return
        
        # Parse scalar
        k = '__'.join(path)
        if k in transformers:
            for typ, k_trg, t in transformers[k]:
                if typ not in container:
                    continue
                if typ == 'info':
                    container['info'][k_trg] = t(data)
                    continue
                if cache is None:
                    continue
                cache[k_trg] = t(data)

        
    
    @property
    def json_data(self) -> dict:
        """Get data"""
        return self._json_data
    
    @property
    def ebucore_data(self) -> dict:
        """Get ebucore data"""
        return self._ebucore_data

    @property
    def data(self) -> dict:
        """Get formated/parsed data"""
        return {
            'hasInfo': self._has_info,
            'hasStreams': self._has_streams,
            'hasAudio': self._has_audio,
            'hasVideo': self._has_video,
            'hasText': self._has_text,
            'hasMenu': self._has_menu,
            
            'info': self._info,
            'audio': self._audio_streams,
            'video': self._video_streams,
            'text': self._text_streams,
            'menu': self._menu_streams,
            
            'default': {
                'audio': self._default_audio_stream,
                'video': self._default_video_stream,
                'text': self._default_text_stream,
                'menu': self._default_menu_stream
            }
        }
    
    
    @property
    def has_info(self) -> bool:
        """Has data"""
        return self._has_info

    @property
    def has_streams(self) -> bool:
        """Has streams"""
        return self._has_streams

    @property
    def has_audio(self) -> bool:
        """Has audio"""
        return self._has_audio

    @property
    def has_video(self) -> bool:
        """Has video"""
        return self._has_video

    @property
    def has_text(self) -> bool:
        """Has text"""
        return self._has_text

    @property
    def has_menu(self) -> bool:
        """Has menu"""
        return self._has_menu


    @property
    def num_streams(self) -> int | None:
        """Number of streams"""
        return self._info['streamCount']

    @property
    def num_video_streams(self) -> int | None:
        """Number of video streams"""
        return self._info['videoCount']

    @property
    def num_audio_streams(self) -> int | None:
        """Number of audio streams"""
        return self._info['audioCount']

    @property
    def num_text_streams(self) -> int | None:
        """Number of text streams"""
        return self._info['textCount']

    @property
    def num_menu_streams(self) -> int | None:
        """Number of menu streams"""
        return self._info['menuCount']


    @property
    def video_format(self) -> str | None:
        """Video format"""
        return self._info['videoFormat']

    @property
    def video_codec(self) -> str | None:
        """Video codec"""
        return self._info['videoCodec']

    @property
    def video_language(self) -> str | None:
        """Video language"""
        return self._info['videoLanguage']


    @property
    def audio_format(self) -> str | None:
        """Audio format"""
        return self._info['audioFormat']

    @property
    def audio_codec(self) -> str | None:
        """Audio codec"""
        return self._info['audioCodec']

    @property
    def audio_language(self) -> str | None:
        """Audio language"""
        return self._info['audioLanguage']


    @property
    def text_format(self) -> str | None:
        """Text format"""
        return self._info['textFormat']

    @property
    def text_codec(self) -> str | None:
        """Text codec"""
        return self._info['textCodec']

    @property
    def text_language(self) -> str | None:
        """Text language"""
        return self._info['textLanguage']


    @property
    def language(self) -> str | None:
        """Language"""
        if self._info['audioLanguage']:
            return self._info['audioLanguage']
        if self._info['videoLanguage']:
            return self._info['videoLanguage']
        return self._info['textLanguage']


    @property
    def file_path(self) -> str | None:
        """Complete name"""
        return self._info['filePath']


    @property
    def folder_name(self) -> str | None:
        """Folder name"""
        return self._info['folderName']

    @property
    def file_name(self) -> str | None:
        """File name"""
        return self._info['fileName']

    #@property
    #def file_name_extension(self) -> str | None:
    #    """File name extension"""
    #    return self._info['fileNameExtension']

    @property
    def file_extension(self) -> str | None:
        """File extension"""
        return self._info['fileExtension']


    @property
    def format(self) -> str | None:
        """Format"""
        return self._info['format']

    @property
    def format_extensions(self) -> str | None:
        """Format extensions"""
        return self._info['formatExtensions']

    @property
    def format_profile(self) -> str | None:
        """Format profile"""
        return self._info['formatProfile']


    @property
    def internet_media_type(self) -> str | None:
        """Internet media type"""
        return self._info['internetMediaType']


    @property
    def codec_id(self) -> str | None:
        """Codec ID"""
        return self._info['codecID']

    @property
    def codec_id_url(self) -> str | None:
        """Codec ID url"""
        return self._info['codecIDUrl']

    @property
    def codec_id_compatible(self) -> str | None:
        """Codec ID Compatible"""
        return self._info['codecIDCompatible']


    @property
    def file_size(self) -> int | None:
        """File size"""
        return self._info['fileSize']

    @property
    def file_size_formated(self) -> str | None:
        """File size formated"""
        return self._info['fileSizeFormated']


    @property
    def duration(self) -> float | None:
        """Duration"""
        return self._info['duration']

    @property
    def duration_formated(self) -> str | None:
        """Duration format"""
        return self._info['durationFormated']

    @property
    def duration_smtp(self) -> str | None:
        """Duration SMTP"""
        return self._info['durationSMTP']


    @property
    def overall_bit_rate_mode(self) -> str | None:
        """Overall bit rate mode"""
        return self._info['overallBitRateMode']

    @property
    def overall_bit_rate(self) -> int | None:
        """Overall bit rate"""
        return self._info['overallBitRate']

    @property
    def overall_bit_rate_unit(self) -> str | None:
        """Overall bit rate unit"""
        return self._info['overallBitRateUnit']


    @property
    def frame_rate(self) -> float | None:
        """Frame rate"""
        return self._info['frameRate']

    @property
    def frame_count(self) -> int | None:
        """Frame count"""
        return self._info['frameCount']


    @property
    def stream_size(self) -> int:
        """Stream size"""
        return self._info['streamSize']

    @property
    def header_size(self) -> int:
        """Header size"""
        return self._info['headerSize']

    @property
    def data_size(self) -> int:
        """Data size"""
        return self._info['dataSize']

    @property
    def footer_size(self) -> int:
        """Footer size"""
        return self._info['footerSize']


    @property
    def is_streamable(self) -> bool:
        """Is streamable"""
        return self._info['isStreamable']


    @property
    def title(self) -> str:
        """Title"""
        return self._info['title']

    @property
    def movie(self) -> str:
        """Movie"""
        return self._info['movie']

    @property
    def performer(self) -> str:
        """Performer"""
        return self._info['performer']

    @property
    def genre(self) -> str:
        """Genre"""
        return self._info['genre']


    @property
    def encoded_date(self) -> str:
        """Encoded date"""
        return self._info['encodedDate']

    @property
    def tagged_date(self) -> str:
        """Tagged date"""
        return self._info['taggedDate']

    @property
    def file_modified_date(self) -> str:
        """File modified date"""
        return self._info['fileModifiedDate']



    @property
    def default_audio_stream(self) -> dict | None:
        """Get default audio stream"""
        if self._default_audio_stream is None:
            return None
        return self._audio_streams[
            self._default_audio_stream
        ]

    @property
    def default_video_stream(self) -> dict | None:
        """Get default video stream"""
        if self._default_video_stream is None:
            return None
        return self._video_streams[
            self._default_video_stream
        ]

    @property
    def default_text_stream(self) -> dict | None:
        """Get default text stream"""
        if self._default_text_stream is None:
            return None
        return self._text_streams[
            self._default_text_stream
        ]

    @property
    def default_menu_stream(self) -> dict | None:
        """Get default menu stream"""
        if self._default_menu_stream is None:
            return None
        return self._menu_streams[
            self._default_menu_stream
        ]



    def audio_stream(self, id: int | None) -> dict | list | None:
        """Audio stream"""
        if id is None:
            return self._audio_streams
        if id < 0 or id >= len(self._audio_streams):
            return self._audio_streams
        return self._audio_streams[id]
    
    @property
    def audio_streams(self) -> list | None:
        return self._audio_streams


    def video_stream(self, id: int | None) -> dict | list | None:
        """Video stream"""
        if id is None:
            return self._video_streams
        if id < 0 or id >= len(self._video_streams):
            return self._video_streams
        return self._video_streams[id]

    @property
    def video_streams(self) -> list | None:
        return self._video_streams


    def text_stream(self, id: int | None) -> dict | list | None:
        """Text stream"""
        if id is None:
            return self._text_streams
        if id < 0 or id >= len(self._text_streams):
            return self._text_streams
        return self._text_streams[id]

    @property
    def text_streams(self) -> list | None:
        return self._text_streams


    def menu_stream(self, id: int | None) -> dict | list | None:
        """Menu stream"""
        if id is None:
            return self._menu_streams
        if id < 0 or id >= len(self._menu_streams):
            return self._menu_streams
        return self._menu_streams[id]

    @property
    def menu_streams(self) -> list | None:
        return self._menu_streams





####################################################################################################
# Testing

if __name__ == '__main__':
    print('Testing TrpMediainfo')
    print('==================')
    print('')
    print('mediainfo_bin:', TrpMediainfo.mediainfo_bin)
    m_info = TrpMediainfo('/home/bx/Videos/Silicon Valley - Season 1-6 [2014]/Season 5/Silicon Valley (2014) - S05E06 - Artificial Emotional Intelligence.mkv')
    #print('mediainfo:', m_info)
    #print('mediainfo.data:', m_info.data)
    #print('')
    #print('mediainfo.ebucore_data:', m_info.ebucore_data)
    #print('')
    #print('mediainfo.audio_streams:', m_info.audio_streams)
    #print('')
    #print('mediainfo.video_streams:', m_info.video_streams)
    #print('')
    #print('mediainfo.text_streams:', m_info.text_streams)
    #print('')
    #print('mediainfo.menu_streams:', m_info.menu_streams)
    #print('')
    print('')
    print('mediainfo.data:', m_info.data)
    print('')


