# Trp MediaMeta

Combines wrappers for `ffprobe` and `mediainfo`.<br>
*Trp FFprobe* and *Trp Mediainfo* can be used separately as well.

## TrpFFprobe

Wrapper around `ffprobe` command.

## TrpMediainfo

Wrapper around `mediainfo` command.

## TrpMediametaToEBUCore

Convert data returned my `TrpMediainfo` to `ebucore` data structure.


## Install

To install it as part of *Transposer* framework, clone it into the *modules* directory of the *Shared Library (shared-lib)*. It will be picked up by *Transposer* components and services automatically.

To install and use it standalone, clone the *Transposer* `utils` modules alongside this one.


### Dependencies

It depends on `ffprobe`, `mediainfo` and the *Transposer* `utils` modules.

Install `requirements.txt`
```bash
pip install -r requirements.txt
```

To install `ffprobe` use `apt` to install `ffmpeg` (on *Debian* based systems). On other systems refer to its package manager's repositories.
```bash
apt install ffmpeg
```

To install `mediainfo` use `apt` (on *Debian* based systems). On other systems refer to its package manager's repositories.
```bash
apt install mediainfo
```

To install the *Transposer* `utils` modules, refer to its documentation.

[git.fairkom.net/transposer/modules/utils](https://git.fairkom.net/emb/displ.eu/transposer/modules/utils)
